from __future__ import unicode_literals

from .models import Inquiry
from django import forms


class ServerForm(forms.ModelForm):
	class Meta:
		model = Inquiry
		widgets = {
			'your_name': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter your name'})
			,'company_name': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter company name'})
			,'email': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter insta'})
			,'country': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter country'})
			,'city': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter city'})
			,'mobile': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter mobile'})
			,'send_quote': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter send_quote'})
			,'contact_me': forms.TextInput(attrs={'class': 'form-control','placeholder':'Enter contact_me'})
		}
		fields = ['your_name','company_name','email','country','city','mobile','send_quote','contact_me']	