# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect, get_object_or_404
from django.shortcuts import render
from .models import Product,Inquiry,CategoryChoices,SubCategoryChoices
from .forms import  ServerForm
from django.http import HttpResponseRedirect
from django.core.mail import EmailMessage
from django.http import JsonResponse
from django.contrib import messages





def home(request, template_name='base.html'):
	
		
	return render(request, template_name)

def homepage(request, template_name='homepage.html'):
	products=Product.objects.filter(product_featured=True,product_show=True)
	product12=Product.objects.all()
	productscat=CategoryChoices.objects.filter(cat_show=True)
	CatChoice=SubCategoryChoices.objects.all()

	
		
	return render(request,template_name,{'products':products,'productscat':productscat,'CatChoice':CatChoice,'product12':product12})

def index(request,id):
	CatChoice=SubCategoryChoices.objects.filter(cat_id=id)

	return render(request,'homepage.html',{'CatChoice':CatChoice})	

def about(request, template_name='about.html'):
	CatChoice=SubCategoryChoices.objects.all()
	productscat=CategoryChoices.objects.filter(cat_show=True)
	products=Product.objects.all()
	featured=Product.objects.filter(product_featured=True,product_show=True)
	product12=Product.objects.all()
	return render(request,template_name,{'products':products,'featured':featured,'productscat':productscat,'CatChoice':CatChoice,'product12':product12})	

def pslug(request,cat_slug):
	article = get_object_or_404(CategoryChoices, cat_slug=cat_slug)
	print article.id
	CatChoice1=SubCategoryChoices.objects.filter(cat_id=article.id)
	CatChoice=SubCategoryChoices.objects.all()
	productscat=CategoryChoices.objects.filter(cat_show=True)
	products=Product.objects.all()
	featured=Product.objects.filter(product_featured=True,product_show=True)
	product12=Product.objects.all()
	return render(request,'pslug.html',{'CatChoice':CatChoice,'CatChoice1':CatChoice1,'products':products,'featured':featured,'productscat':productscat,'CatChoice':CatChoice,'product12':product12})



def proslug(request,cat_slug,sub_slug):
	article = get_object_or_404(SubCategoryChoices, sub_slug=sub_slug)
	print article.cat_id
	pchoice=Product.objects.filter(sub_cat_id=article.id)

	CatChoice=SubCategoryChoices.objects.all()
	productscat=CategoryChoices.objects.filter(cat_show=True)
	products=Product.objects.all()
	featured=Product.objects.filter(product_featured=True,product_show=True)
	product12=Product.objects.all()
	return render(request,'proslug.html',{'pchoice':pchoice,'products':products,'featured':featured,'productscat':productscat,'CatChoice':CatChoice,'product12':product12})


def products(request,template_name='product.html'):
	productscat=CategoryChoices.objects.filter(cat_show=True)
	products=Product.objects.all()
	featured=Product.objects.filter(product_featured=True,product_show=True)
	return render(request,template_name,{'products':products,'featured':featured,'productscat':productscat})

def post1(request,sub_slug,cat_slug,product_slug):
	article = get_object_or_404(Product, product_slug=product_slug)
	return render(request,'post.html',{'article':article})


def post(request,product_slug):
	article = get_object_or_404(Product, product_slug=product_slug)
	return render(request,'post.html',{'article':article})


def create(request):
	data = request.POST.dict()
	Inquiry.objects.create(your_name=data['name'], company_name=data['company_name'], email=data['email'], country=data['country']
		, city=data['city'], mobile=data['mobile'], send_quote=data['send_quote'],contact_me=data['contact_me'],product_id=data['product_id'])
	return redirect('products')


def contact(request, template_name='contact.html'):
	
	success1=""
	CatChoice=SubCategoryChoices.objects.all()
	productscat=CategoryChoices.objects.filter(cat_show=True)
	products=Product.objects.all()
	featured=Product.objects.filter(product_featured=True,product_show=True)
	product12=Product.objects.all()
	data = request.POST.dict()
	if data:
		body='''<div style="max-width:560px; text-align:center; margin: 0 auto; "><div style="float:left; width: 100%;border:solid 1px #e4e4e4; padding:25px; "><img src="http://aceinternationaltrading.com/static/images/ace-logo.png" width="250" style="margin: 0 auto;"><div style="padding:20px 0; text-align:center; background:#009647; font-weight: bolder; font-size: 17px; float:left; width: 100%; color:#ffffff;">You have got an Enquiry!</div> <div style="float:left; width:100%; padding: 20px 5px"><p style="padding-bottom:10px; float:left; text-align:left; width:100%">You have got an enquiry from <b>'''+data['name']+'''('''+data['Company']+''')</b>.</p><p style="padding-bottom:10px; text-align: left; float:left; text-align:justify; width: 100%"><b>Message:</b> '''+data['Message']+'''</p><p style="padding-bottom:10px; text-align: left; float:left; width: 100%"><b>Contact Details: </b>'''+data['Email']+''', '''+data['Mobile']+'''</p></div></div></div>'''
		email = EmailMessage('Contact', body, to=["ishmeet@aceinternationaltrading.com","raymond@aceinternationaltrading.com"])
		#email = EmailMessage('Contact', body, to=["jatinderharjinder95@gmail.com"])
		email.content_subtype = "html"
		email.send()
		messages.add_message(request, messages.INFO, 'Message delivered Succesfully')
	return render(request,template_name,{'Success':success1,'products':products,'featured':featured,'productscat':productscat,'CatChoice':CatChoice,'product12':product12})



