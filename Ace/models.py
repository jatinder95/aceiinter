
from __future__ import unicode_literals

from django.db import models



class CategoryChoices(models.Model):
    class Meta:
        verbose_name_plural = 'categorychoices'
    cat_name = models.CharField(default="",max_length=50)
    cat_slug = models.SlugField(max_length=100,null=True,unique=True)
    cat_image= models.ImageField(upload_to='cat_image',blank=True)
    cat_show = models.BooleanField(default=False)

    def __str__(self):
        return self.cat_name


class SubCategoryChoices(models.Model):
    class Meta:
        verbose_name_plural = 'SubCategoryChoices'
    sub_cat_name = models.CharField(default="",max_length=50)
    cat_id=models.IntegerField(default=1,blank=True, null=True)
    sub_slug = models.SlugField(max_length=100,null=True,unique=True)
    sub_image= models.ImageField(upload_to='sub_image',blank=True)



    def __str__(self):
        return self.sub_cat_name


class Product(models.Model):
  product_name = models.CharField(max_length=500,null=True)
  product_details = models.CharField(max_length=500,null=True)
  product_image= models.ImageField(upload_to='prod_image',blank=True)
  product_slug = models.SlugField(max_length=100,null=True,unique=True)
  product_keywords = models.CharField(max_length=100,null=True)
  product_description = models.TextField(max_length=8000)
  product_featured=models.BooleanField(default=False)
  cat_id=models.IntegerField(default=1,blank=True, null=True)
  sub_cat_id=models.IntegerField(default=1,blank=True, null=True)
  product_show=models.BooleanField(default=False)



	

class Inquiry(models.Model):
	your_name = models.CharField(max_length=500,null=True)
	company_name = models.CharField(max_length=500,null=True)
	email= models.EmailField(blank=True, unique=False)
	country = models.CharField(max_length=500,null=True)
	city = models.CharField(max_length=500,null=True)
	mobile=models.IntegerField(blank=True, null=True)
	send_quote=models.IntegerField(blank=True, null=True)
	contact_me=models.IntegerField(blank=True, null=True)
	product_id = models.IntegerField(blank=True, null=True)
	created_at = models.DateTimeField(auto_now=True)










