from django.conf.urls import url, include
from django.contrib.auth import views as auth_views


from Ace import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^demo/$', views.homepage),
    url(r'^products/$', views.products,name='products'),
    url(r'^products/(?P<product_slug>[\w-]+)/$', views.post),
     url(r'^index/(?P<id>[0-9]+)', views.index),
   # url(r'^create/(?P<id>\d+)/', views.create),
    url(r'^contact/$', views.contact,name='contact'),
    url(r'^about/$', views.about,name='about'),
    url(r'^create', views.create),
    url(r'^product/(?P<cat_slug>[\w-]+)/$', views.pslug),
    url(r'^product/(?P<cat_slug>[\w-]+)/(?P<sub_slug>[\w-]+)/$', views.proslug),
    url(r'^product/(?P<cat_slug>[\w-]+)/(?P<sub_slug>[\w-]+)/(?P<product_slug>[\w-]+)', views.post1),

 
   
]
