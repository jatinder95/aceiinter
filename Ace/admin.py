# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from Ace.models import Product, Inquiry,CategoryChoices,SubCategoryChoices

@admin.register(Product)
class RateAdmin(admin.ModelAdmin):
	fields = ('product_name','product_show','product_details','product_image', 'product_slug', 'product_keywords','product_description','product_featured','cat_id','sub_cat_id')
	list_display = ['product_name','product_show', 'product_details','product_image', 'product_slug', 'product_keywords','product_description','product_featured','cat_id','sub_cat_id']
	search_fields = ('product_name',)



@admin.register(Inquiry)
class RateAdmin(admin.ModelAdmin):
	fields = ('your_name', 'company_name','email', 'country','city', 'mobile','send_quote','contact_me','product_id')
	list_display = ['your_name', 'company_name','email', 'country','city', 'mobile','send_quote','contact_me','product_id','created_at']
	search_fields = ('your_name',)





@admin.register(CategoryChoices)
class RateAdmin(admin.ModelAdmin):
	fields = ('cat_name','cat_slug','cat_image','cat_show')
	list_display = ['cat_name','cat_slug','cat_image','cat_show']
	search_fields = ('cat_name',)



@admin.register(SubCategoryChoices)
class RateAdmin(admin.ModelAdmin):
	fields = ('sub_cat_name','cat_id','sub_slug','sub_image')
	list_display = ['sub_cat_name','cat_id','sub_slug','sub_image']
	search_fields = ('sub_cat_name',)